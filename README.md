## test full-stack Clever IT

este proyecto fue creado como prueba de conocimientos
para el cargo de full stack java.

Se desarrollo una Api y un frontend en donde la api permite lo siguiente:
 - extraer matriculas de auto de una api rest remota y guardarlas en una base de datos local.
 - listar todas las matriculas obtenidas localmente.
 - pruebas unitarias con el siguiente coverage (class: 66%, method: 81%, lines: 75%)
 
 para la api se uso MariaDB y Java con Spring Boot.
 
 el frontend permite lo siguiente:
 - operaciones CRUD a una api remota de usuarios.
 - interfaz con AJAX para que los cambios se vean reflejados sin reiniciar el explorador.
 
 para el frontend se uso Angular 9.0
 
 #  Configuración del ambiente 
 Es necesario tener instalado npm, java, docker y docker-compose para seguir estos pasos.
 
 Se recomienda usar Intellij para Spring boot.
 
 Para levantar la base de dato utilizaremos el archivo docker-compose.yml.
 El cual contiene una base de datos MariaDB y PhpMyAdmin como visualizador de la bd, tambíen posee las credenciales de estas tecnologías.
 ```
 docker-compose up -d
 ```
Maria DB utilizará el puerto 3306.
PhpMyAdmin utilizará el puerto 5200.

Instalamos los paquetes de la api

```
mvn install
```

Instalamos las librerias del frontend:

```
cd /src/main/resources/frontend
npm i
```

## Para levantar el backend ##
```
./mvnw spring-boot:run
```

## Para levantar el frontend ##
```
cd /src/main/resources/frontend
ng serve
```
