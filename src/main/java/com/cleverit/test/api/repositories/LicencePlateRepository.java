package com.cleverit.test.api.repositories;

import com.cleverit.test.api.models.LicencePlate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LicencePlateRepository extends JpaRepository<LicencePlate, Integer> {
}
