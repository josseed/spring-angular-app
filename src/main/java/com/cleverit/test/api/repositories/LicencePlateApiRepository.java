package com.cleverit.test.api.repositories;

import com.cleverit.test.api.models.LicencePlate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Repository
public class LicencePlateApiRepository {
    @Value("${licence.plate.url}")
    private String API;

    private final RestTemplate restTemplate;

    public LicencePlateApiRepository() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        this.restTemplate = restTemplateBuilder.build();
    }

    public List<LicencePlate> getLicencesPlate() {
        ResponseEntity<LicencePlate[]> response = this.restTemplate.getForEntity(API, LicencePlate[].class);
        if(response.getStatusCode() == HttpStatus.OK) {
            return Arrays.asList(response.getBody());
        } else {
            return null;
        }
    }
}