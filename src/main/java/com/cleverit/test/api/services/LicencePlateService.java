package com.cleverit.test.api.services;

import com.cleverit.test.api.models.LicencePlate;
import com.cleverit.test.api.repositories.LicencePlateApiRepository;
import com.cleverit.test.api.repositories.LicencePlateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LicencePlateService {

    @Autowired
    private LicencePlateApiRepository licencePlateApiRepository;

    @Autowired
    private LicencePlateRepository licencePlateRepository;

    public List<LicencePlate> fetchRemoteLicencesAndSave() {
        List<LicencePlate> licencePlates = this.licencePlateApiRepository.getLicencesPlate();
        licencePlates.forEach(this.licencePlateRepository::save);
        return licencePlates;
    }

    public List<LicencePlate> getAllLicencePlates() {
        List<LicencePlate> licencePlates = new ArrayList<LicencePlate>();
        licencePlateRepository.findAll().forEach(licencePlates::add);
        return licencePlates;
    }
}