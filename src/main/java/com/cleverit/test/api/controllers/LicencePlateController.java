package com.cleverit.test.api.controllers;

import com.cleverit.test.api.models.LicencePlate;
import com.cleverit.test.api.services.LicencePlateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
@RequestMapping("/api/v1/licence-plates")
@RestController
public class LicencePlateController {
    /*
     * This use case make a get request to a remote api of a lists of licence plates and
     * convert the content to a List<LicencePlate> and finally is saved in a
     * database MariaDB. this return a 200 status code with a list of all licence plates as
     * json array.
     */

    @Autowired
    private LicencePlateService licencePlateService;

    @GetMapping(path="/fetch-remote", produces = "application/json")
    public ResponseEntity<List<LicencePlate>> fetch() {
        try {
            List<LicencePlate> licencePlates = this.licencePlateService.fetchRemoteLicencesAndSave();
            if (licencePlates.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(licencePlates, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<LicencePlate>> getAllLicencePlates() {
        try {
            List<LicencePlate> licencePlates = new ArrayList<LicencePlate>();

            licencePlates = licencePlateService.getAllLicencePlates();

            if (licencePlates.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(licencePlates, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}