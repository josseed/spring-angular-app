package com.cleverit.test.api.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Id;
import com.google.api.client.util.Key;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class LicencePlate {

    @Id
    @Key
    private String id;

    @Key
    private String plate;

    @Key
    private String type;

    @Key
    private String color;

}