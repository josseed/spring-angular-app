import { Injectable} from "@angular/core";
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Environment } from 'src/environments/environment';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: "root"
})
export class UserService {

  private api: string;

  constructor( private http: HttpClient, private router: Router) {
    this.api = Environment.api;
  }

  private headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  public getUsers(): Observable<any> {
    const url_api = `${this.api}/user`;
    return this.http.get(url_api, { headers: this.headers });
  }

  public createUser(user: User): Observable<any> {
    const url_api = `${this.api}/user`;
    return this.http.post(url_api, user.toJSON(), { headers: this.headers });
  }

  public getUser(id: number): Observable<any> {
    const url_api = `${this.api}/user/${id}`;
    return this.http.get(url_api, { headers: this.headers });
  }

  public editUser(id: number, user: User): Observable<any> {
    const url_api = `${this.api}/user/${id}`;
    return this.http.patch(url_api, user.toJSON(), { headers: this.headers });
  }

  public deleteUser(id: number): Observable<any> {
    const url_api = `${this.api}/user/${id}`;
    return this.http.delete(url_api, { headers: this.headers });
  }

}