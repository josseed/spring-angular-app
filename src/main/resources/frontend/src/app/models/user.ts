
export class User {

  public id: number;
  public first_name: string;
  public last_name: string;
  public email: string;
  public profession: string;


  constructor(data: any = null) {
    if (data) {
      this.id = data.id;
      this.first_name = data.nombre;
      this.last_name = data.apellido;
      this.email = data.email;
      this.profession = data.profesion;
    }
  }

  public toJSON(): any {
    return {
      id: this.id,
      nombre: this.first_name,
      apellido: this.last_name,
      email: this.email,
      profesion: this.profession
    }
  }
}