import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { NzNotificationService } from 'ng-zorro-antd';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private notification: NzNotificationService,
    private userService: UserService
  ) { }
  
  private routeSub: Subscription;
  private menuId: number;
  public users: User[];
  public newUser: User;
  public isLoading: boolean = true;
  public isVisible: boolean = false;
  public pageSize = 5;
  public editId: number | null = null;

  public ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.menuId = params['id'];
      this.getUsers();
    });
  }

  private getUsers(): void {
    this.userService.getUsers().subscribe(data => {
      this.users = data.map(user => new User(user));
      this.isLoading = false;
    })
  }

  public ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  public createUser(): void {
    this.userService.createUser(this.newUser).subscribe(data => {
      let user = new User(data);
      //ading the new user without reloading the browser.
      this.users = [...this.users, user];
      this.userCreated('success');
    })
  }

  public showModal(): void {
    this.newUser = new User();
    this.isVisible = true;
  }

  public handleCancel(): void {
    this.isVisible = false;
  }

  public deleteRow(id: number): void {
    this.userService.deleteUser(id).subscribe(data => {
      this.userDeleted('success');
      //deleting the user without reloading the browser.
      this.users = this.users.filter(user => user.id !== id);
    });
  }

  public startEdit(id: number): void {
    this.editId = id;
  }

  public stopEdit(): void {
    let userEdited = this.users.find(user => user.id === this.editId);
    this.userService.editUser(this.editId, userEdited).subscribe(data => {
      this.userEdited('success');
    })
    this.editId = null;
  }

  private userEdited(type: string): void {
    this.notification.create(
      type,
      'Success:',
      'Usuario editado correctamente.'
    );
  }

  private userCreated(type: string): void {
    this.notification.create(
      type,
      'Success:',
      'Usuario creado correctamente.'
    );
  }

  private userDeleted(type: string): void {
    this.notification.create(
      type,
      'Success:',
      'Usuario eliminado correctamente.'
    );
  }
}
