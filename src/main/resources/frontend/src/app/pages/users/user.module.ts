import { NgModule } from '@angular/core';

import { UserRoutingModule } from './user.routing.module';
import { NzLayoutModule, NzMenuModule, NgZorroAntdModule } from 'ng-zorro-antd';
import { UserComponent } from './user.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    UserRoutingModule,
    FormsModule,
    NzLayoutModule,
    NzMenuModule,
    HttpClientModule,
    CommonModule,
    NgZorroAntdModule
  ],
  declarations: [UserComponent],
  exports: [UserComponent]
})
export class UserModule { }
