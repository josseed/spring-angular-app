package com.cleverit.test.api.models;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class LicencePlateTest {
    /*
     * This test has the following steps:
     * create a instance of LicencePlate without args and expect null in all the attributes.
     * create a instance of LicencePLate with args and expect the correct value in all the attributes.
     * build a instance of LicencePlate and expect the correct value in all the attributes.
     */
    private LicencePlate licencePlate;

    @Before
    public void setup() {

        this.licencePlate = new LicencePlate("1", "fghk24", "vagon", "white");
    }

    @Test
    public void testNewInstanceWithoutArgs() {
        licencePlate = new LicencePlate();
        assertEquals(this.licencePlate.getId(), null);
        assertEquals(this.licencePlate.getPlate(), null);
        assertEquals(this.licencePlate.getColor(), null);
        assertEquals(this.licencePlate.getType(), null);
    }

    @Test
    public void testNewInstanceWithArgs() {
        licencePlate = new LicencePlate("2", "gggg21", "car", "pink");
        assertEquals(this.licencePlate.getId(), "2");
        assertEquals(this.licencePlate.getPlate(), "gggg21");
        assertEquals(this.licencePlate.getColor(), "pink");
        assertEquals(this.licencePlate.getType(), "car");
    }

    @Test
    public void testBuilder() {
        licencePlate = LicencePlate.builder()
                .id("3")
                .plate("ffff42")
                .type("car")
                .color("pink")
                .build();
        assertEquals(this.licencePlate.getId(), "3");
        assertEquals(this.licencePlate.getPlate(), "ffff42");
        assertEquals(this.licencePlate.getColor(), "pink");
        assertEquals(this.licencePlate.getType(), "car");
    }

    @Test
    public void testGetters() {
        assertEquals(this.licencePlate.getId(), "1");
        assertEquals(this.licencePlate.getPlate(), "fghk24");
        assertEquals(this.licencePlate.getColor(), "white");
        assertEquals(this.licencePlate.getType(), "vagon");
    }

    @Test
    public void testSetters() {
        this.licencePlate.setId("4");
        assertEquals(this.licencePlate.getId(), "4");
        this.licencePlate.setPlate("ffff25");
        assertEquals(this.licencePlate.getPlate(), "ffff25");
        this.licencePlate.setColor("magenta");
        assertEquals(this.licencePlate.getColor(), "magenta");
        this.licencePlate.setType("truck");
        assertEquals(this.licencePlate.getType(), "truck");
    }
}
