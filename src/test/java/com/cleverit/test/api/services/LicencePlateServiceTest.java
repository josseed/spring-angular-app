package com.cleverit.test.api.services;


import com.cleverit.test.api.models.LicencePlate;
import com.cleverit.test.api.repositories.LicencePlateApiRepository;
import com.cleverit.test.api.repositories.LicencePlateRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

public class LicencePlateServiceTest {

    @Mock
    private LicencePlateRepository licencePlateRepository;
    @Mock
    private LicencePlateApiRepository licencePlateApiRepository;
    private List<LicencePlate> licencePlates;
    private List<LicencePlate> db;
    private LicencePlateService licencePlateService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        licencePlateService = new LicencePlateService();
        ReflectionTestUtils.setField(licencePlateService, "licencePlateApiRepository", licencePlateApiRepository);
        ReflectionTestUtils.setField(licencePlateService, "licencePlateRepository", licencePlateRepository);

        this.licencePlates = new ArrayList<LicencePlate>();
        this.db = new ArrayList<LicencePlate>();

        when(licencePlateApiRepository.getLicencesPlate()).thenReturn(licencePlates);
        when(licencePlateRepository.findAll()).thenReturn(licencePlates);
        doAnswer(invocation -> {
            db.add((LicencePlate) invocation.getArguments()[0]);
            return invocation.getArguments()[0];
        }).when(licencePlateRepository).save(Mockito.any(LicencePlate.class));
    }

    @Test
    public void testFetchRemoteAddOneLicencePlate() {
        licencePlates.add(new LicencePlate());
        assertEquals(this.licencePlateService.fetchRemoteLicencesAndSave().size(), 1);
    }

    @Test
    public void testFetchRemoteAddTwoLicencePlates() {
        licencePlates.add(new LicencePlate());
        licencePlates.add(new LicencePlate());
        assertEquals(this.licencePlateService.fetchRemoteLicencesAndSave().size(), 2);
    }

    @Test
    public void testFetchRemoteEmpty() {
        assertEquals(this.licencePlateService.fetchRemoteLicencesAndSave().size(), 0);
    }

    @Test
    public void testGetAllLicencePlates() {
        licencePlates.add(new LicencePlate());
        licencePlates.add(new LicencePlate());
        licencePlates.add(new LicencePlate());
        assertEquals(this.licencePlateService.getAllLicencePlates().size(), 3);

    }

    @Test
    public void testGetAllLicencePlatesEmpty() {
        assertEquals(this.licencePlateService.getAllLicencePlates().size(), 0);

    }
}