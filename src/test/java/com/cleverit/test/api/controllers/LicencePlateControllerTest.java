package com.cleverit.test.api.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import com.cleverit.test.api.models.LicencePlate;
import com.cleverit.test.api.services.LicencePlateService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class LicencePlateControllerTest {
    @Mock
    private LicencePlateService licencePlateService;
    private LicencePlateController controller;
    private List<LicencePlate> licencePlates;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        controller = new LicencePlateController();
        ReflectionTestUtils.setField(controller, "licencePlateService", licencePlateService);
        licencePlates = new ArrayList<LicencePlate>();
    }

    @Test
    public void testFetchAndSave() {
        licencePlates.add(new LicencePlate());
        when(licencePlateService.fetchRemoteLicencesAndSave()).thenReturn(this.licencePlates);
        ResponseEntity<List<LicencePlate>> result = controller.fetch();
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(result.getBody().size(), 1);
    }

    @Test
    public void testFetchAndSaveNoContent() {
        when(licencePlateService.fetchRemoteLicencesAndSave()).thenReturn(this.licencePlates);
        ResponseEntity<List<LicencePlate>> result = controller.fetch();
        assertEquals(result.getStatusCode(), HttpStatus.NO_CONTENT);
    }

    @Test
    public void testFetchAndSaveServerError() {
        when(licencePlateService.fetchRemoteLicencesAndSave()).thenReturn(null);
        ResponseEntity<List<LicencePlate>> result = controller.fetch();
        assertEquals(result.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testGetAllLicencePlates() {
        licencePlates.add(new LicencePlate());
        when(licencePlateService.getAllLicencePlates()).thenReturn(this.licencePlates);
        ResponseEntity<List<LicencePlate>> result = controller.getAllLicencePlates();
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(result.getBody().size(), 1);
    }

    @Test
    public void testGetAllLicencePlatesNoContent() {
        when(licencePlateService.getAllLicencePlates()).thenReturn(this.licencePlates);
        ResponseEntity<List<LicencePlate>> result = controller.getAllLicencePlates();
        assertEquals(result.getStatusCode(), HttpStatus.NO_CONTENT);
    }

    @Test
    public void testGetAllLicencePlatesServerError() {
        when(licencePlateService.getAllLicencePlates()).thenReturn(null);
        ResponseEntity<List<LicencePlate>> result = controller.getAllLicencePlates();
        assertEquals(result.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
